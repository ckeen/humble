LATEX ?= lualatex

humble.pdf: humble.tex
	${LATEX} $<
	${LATEX} $<
	${LATEX} $<

default: humble.pdf

.PHONY: clean

clean:
	rm *.log *.aux *.out *.idx *.lol *.pdf *.toc || /usr/bin/true
